package com.richdyang.tutorials.jax_rs_cxf;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;


@Path("/")
@Produces("application/xml")
public class UserResource {
    @GET
    @Path("/user/{userName}")
    public User greetUser(@PathParam("userName") String userName) {
        User user = new User();
        user.setName(userName);
        user.setAge((short) 32);
        return user;
    }
}



